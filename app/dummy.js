require('../newrelic');


function registerHandler(request, reply) {
  'use strict';

  require('../newrelic');

  const payload = request.payload;

  if (payload.email === '' || payload.password === '') {
    throw new Error('Error thrown');
  }

  reply({
    success: true
  })
}

function register(server, options, next) {
  'use strict';

  server.route({
    method: 'POST',
    path: '/register',
    handler: registerHandler
  });

  return next();
}

register.attributes = {
  name: 'api'
};

module.exports = register;
